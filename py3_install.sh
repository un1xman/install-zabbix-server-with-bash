#!/bin/bash
mkdir /opt/python3.x && cp tools/Python-3.7.4.tgz /opt/python3.x && cd /opt/python3.x
tar xzf Python-3.7.4.tgz
/opt/python3.x/Python-3.7.4/configure --enable-optimizations
make altinstall
cd /root/install-zabbix-server-with-bash
