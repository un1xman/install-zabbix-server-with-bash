#!/bin/bash
#cat packages.txt | while read line
#do
#	yum install -y  $line
#done
echo "# Start hhtpd"
systemctl start httpd
systemctl enable httpd
echo "# Start mariadb"
systemctl start mariadb
systemctl enable mariadb
echo "# Config firewall"
firewall-cmd --permanent --add-service=http
systemctl restart firewalld
